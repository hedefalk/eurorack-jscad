const panelThickness = 2.3

const { widthOfHp, eurorackPanel, PJ301, AlphaPot, ThreeMmLed, PowerSwitch } = require('./eurorack-lib/eurorack-lib')({
  panelThickness,
})

const jscad = require('jscad-tree-experiment')
const { difference, union } = jscad.api.booleanOps
const { translate, rotate, mirror } = jscad.api.transformations
const { cube, cylinder } = jscad.api.primitives3d
const { color } = jscad.api.color

const panelHp = 14
const width = widthOfHp(panelHp)

const middle = width / 2

// No margin
const grillY = 61 + 2
const grillX = 56 + 2

const speakerOuterDiam = 57
const speakerHoleDiam = 50

const grillMounts = () => {
  const grillXMount = 56
  const grillYMount = 41
  const grillMountLength = 7 // with margin

  const slit = cube({ size: [1, grillMountLength, 20], center: true })

  const slitExtra = cube({ size: [5, grillMountLength, 20] })

  // slit = cube({ size: [1, grillMountLength, 20], center: true }) // COMMENT OUT FOR ISSUE

  return union(
    translate([-grillXMount / 2, -grillYMount / 2, 0], slit, slitExtra),
    translate([+grillXMount / 2, -grillYMount / 2, 0], slit, slitExtra),
    translate([-grillXMount / 2, +grillYMount / 2, 0], slit, slitExtra),
    translate([+grillXMount / 2, +grillYMount / 2, 0], slit, slitExtra)
  )
}

const speakerMountCircle = () => {
  return difference(
    cylinder({ d: speakerOuterDiam + 5, h: 5, $fn: 40 }),
    cylinder({ d: speakerOuterDiam, h: 5, $fn: 40 })
  )
}

const speakerHole = () => {
  return translate([0, 0, -0.01], cylinder({ d: speakerHoleDiam, h: 20, $fn: 40 }))
}

const speakerY = 45

function negatives() {
  return union(
    translate([middle, speakerY, 0], speakerHole()),
    translate(
      [middle, speakerY, 0],
      translate([0, 0, panelThickness], cube({ size: [grillX, grillY, 1], center: true }))
    ),
    translate([middle, speakerY, 0], translate([0, 0, -0.1], cube({ size: [grillX, grillY, 1], center: true }))),
    translate([middle, 90, 0], AlphaPot()),
    translate([middle - 20, 110, 0], PJ301()),
    translate([middle + 10, 110, 0], ThreeMmLed()),
    translate([middle + 20, 110, 0], PowerSwitch())
  )
}

function main() {
  return difference(
    union(
      difference(union(eurorackPanel(panelHp, { mountHoles: 4, slots: false })), negatives()),
      translate([middle, speakerY, 0], speakerMountCircle())
    ),
    translate([middle, speakerY, 0], grillMounts())
  )
}

module.exports = { main, getParameterDefinitions: () => [] }
