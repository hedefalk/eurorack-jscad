const {hp, m3, panelOuterHeight: threeUPanelOuterHeight, oneUOuterHeight, 
  panelThickness: defaultPanelThickness, panelThicknessAtRail, mountHoleXMargin, offsetToMountHoleCenterY,
  railClearence} = require('./constants')
// const jscad = require('jscad-tree-experiment')

const jscadApi = require('@jscad/csg/api')
const {cube, cylinder} = jscadApi.primitives3d
const {translate} = jscadApi.transformations
const {difference, union} = jscadApi.booleanOps
// const color = jscad.api.color.color

// const holeWidth = 5.08
const mountHoleDiameter = m3
const mountHoleRad = mountHoleDiameter / 2

module.exports = params => { 
  const defaults = {
    panelThickness: defaultPanelThickness,
  }

  const options = Object.assign({}, defaults, params)

  const panelThickness = options.panelThickness 
  const mountHoleDepth = panelThickness + 2

  function roundHole(diam) {
    return translate([0, 0, -0.01], cylinder({d: diam, h: mountHoleDepth+2, $fn: 40}))
  }

  function widthOfHp(panelHp) {
    return panelHp * hp - 0.4 
  }

  function eurorackPanel(panelHp, params) {
    
    const defaults = {
      mountHoles: 4,
      mountHoleSlotWidth: 6,
      slots: true,
      panelOuterHeight: threeUPanelOuterHeight
    }
    
    const options = Object.assign({}, defaults, params)
    
  
    // For 2HP
    function smallMountHole() {
      return translate([0,0,-1],
        cylinder({d: mountHoleDiameter, h: mountHoleDepth, center: false}))
    }
    const panelOuterHeight = options.panelOuterHeight
    
    const mountHoleCubeWidth = options.mountHoleSlotWidth - mountHoleDiameter
    
  
    function eurorackMountHole() {
      const mountHoleDepth = panelThickness + 2 //because diffs need to be larger than the object they are being diffed from for ideal BSP operations
      
      return translate([0,0,-1],
        cylinder({d: mountHoleDiameter, h: mountHoleDepth, center: false}),
        translate([0,0-mountHoleRad,0],
          cube({size: [mountHoleCubeWidth, mountHoleDiameter, mountHoleDepth], center: false})),
        translate([mountHoleCubeWidth,0,0], 
          cylinder({d: mountHoleDiameter, h: mountHoleDepth, center: false}))
      )
    }
  
    
    // Actual width just a bit less than calculated for margin
    // 0.4 is about average of the table numbers given by Doepfer 
    // http://www.doepfer.de/a100_man/a100m_e.htm
    // To me it doesn't really make sense to use that table though
    const width = widthOfHp(panelHp)
    
    const mainPanel = cube({size: [width, panelOuterHeight, panelThickness], center: false})
    
    const mountingCutout = 
      translate([0, 0, panelThicknessAtRail+0.001], union(
        cube({size: [hp*panelHp, railClearence, panelThickness-panelThicknessAtRail], center: false}),
        translate([0, panelOuterHeight-railClearence, 0],
          cube({size: [hp*panelHp, railClearence, panelThickness-panelThicknessAtRail], center: false}))
      ))
      
    const holes = options.mountHoles 
  
    const offsetToMountHoleCenterX = mountHoleCubeWidth > 0 ? 
      mountHoleXMargin - mountHoleCubeWidth / 2 
      : mountHoleXMargin
      
    // Doepfer says 7.5 from left side, we can't use same offset from the right side since not a multiple of hp
    // See http://www.doepfer.de/a100_man/a100m_e.htm
    const rightMountHoleOffsetX = offsetToMountHoleCenterX + hp * (panelHp-3)
    
    // Does not yet support more than 4 holes, will fix this iff I do anything bigger
    const mountHolesCutouts = (() => {
      if(panelHp <= 2) {
        return [
          translate([mountHoleXMargin - hp, panelOuterHeight - offsetToMountHoleCenterY, 0],
            smallMountHole()),
          translate([mountHoleXMargin - hp, offsetToMountHoleCenterY,0],
            smallMountHole())
        ]
      } else {
        const cutouts = [
          // top left
          translate([offsetToMountHoleCenterX, panelOuterHeight - offsetToMountHoleCenterY, 0],
            eurorackMountHole()),
          // bottom left
          translate([offsetToMountHoleCenterX, offsetToMountHoleCenterY,0],
            eurorackMountHole())
        ]
            
        if(holes>2) {
          //topRight
          cutouts.push(translate([rightMountHoleOffsetX, panelOuterHeight - offsetToMountHoleCenterY,0],
            eurorackMountHole()))
          //bottomRight
          cutouts.push(translate([rightMountHoleOffsetX, offsetToMountHoleCenterY,0],
            eurorackMountHole()))
        }
  
        return translate([0, 0, 1], cutouts)
      }
    })()
    
    const cutouts = [mountHolesCutouts]
    if(options.slots) {
      cutouts.push(mountingCutout)
    }
    return difference(mainPanel, cutouts)
  }

  return {
    hp,
    widthOfHp,
    eurorackPanel,

    oneUEurorackPanel: (panelHp, options = {}) => {
      return eurorackPanel(panelHp, Object.assign({}, options, {panelOuterHeight: oneUOuterHeight}))
    },
  
    // Square and cirular cutout to make the protrude a bit more
    // I have a big batch of defective ones with short threads so need that extra
    PJ301: (params) => {
      const defaultParams = {
        extraCutouts: true,
        extraCutoutDepth: 2.1,
      }
    
      const options = Object.assign({}, defaultParams, params)

      const jackHoleDiam = 6.5
      const extra_cutting_upwards = 10

      const result = [roundHole(jackHoleDiam)]

      if(options.extraCutouts) {
        result.push(translate([0, 0, panelThickness-1.5],
          cylinder({d: 8.5, h:1.4, $fn:40})))
        result.push(translate([0, 0.7, panelThickness + extra_cutting_upwards/2],
          cube({size: [9.5, 11, 2.1 + extra_cutting_upwards], center: true})))
      }
      return result
    },

    ThreeMmLed: () => {
      return translate([0, 0, -0.01],
        cylinder({d:3.4, h:mountHoleDepth+2, $fn:40}))
    },

    RV09Pot: () => {
      const potHoleDiam = 7
      return translate([0, 0, -0.01], cylinder({d: potHoleDiam, h: mountHoleDepth+2, $fn: 40}))
    },

    AlphaPot: () => {
      return translate([0, 0, -0.01], cylinder({d: 7.5, h: mountHoleDepth+2, $fn: 40}))
    },
    


    ToggleSwitchDPDT: () => {
      const extraCutoutDepth = 2.2
      return union(
        roundHole(7.5),
        translate([0, 0, panelThickness], cube({size: [13, 13, extraCutoutDepth*2], center: true}))
      )
    },
  
    ToggleSwitchSPDT: () => {
      const extraCutoutDepth = 2.2
      return union(
        roundHole(7.5),
        translate([0, 0, panelThickness], cube({size: [8.5, 13, extraCutoutDepth*2], center: true}))
      )
    },

    // A small power switch
    PowerSwitch: () => {
      // outer 10.35*15
      const width = 8.5
      const length = 13.5
      return translate([0, 0, -0.01], cube({size: [width, length, mountHoleDepth+2]}))
    },

    TrimmerHole: () => {
      const holeDiam = 3.5
      return translate([0, 0, -0.01], cylinder({d: holeDiam, h: mountHoleDepth+2, $fn: 40}))
    },

    usbTypeA: () => {
      return translate([0, 0, -0.01], cube({size: [15, 7, mountHoleDepth+2]}))
    },

    sideSupport: (width = 2.5, height = 20) => {
      return translate([0, railClearence, 0], cube({size: [width, 21, height], center: false}))
    }
  }}
