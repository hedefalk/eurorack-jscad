const jscad = require("jscad-tree-experiment")
const { translate } = jscad.api.transformations
const { cube, cylinder } = jscad.api.primitives3d
const { difference, union } = jscad.api.booleanOps
const {
  threeUHeight,
  oneU,
  panelOuterHeight,
  oneUOuterHeight,
  offsetToMountHoleCenterY,
} = require("../eurorack-lib/constants")

const width = 30.1 // just because the others were. Should be 30.1 for git-tec https://synthracks.com/eurorack-rails
// const yBetweenHoles = 33.65
// Distance between holes should be exactly the same as distance between mounting holes of front panels

function main(params) {
  const thickness = params.thickness
  const railsDrillHole = (() => {
    const dk = 10.5
    const d = 5.5
    const dkMargin = 0.5
    const headHeight = (dk + dkMargin - d) / 2 // angle is 45°

    return union(
      cylinder({ d, h: thickness + 0.0001 }),
      translate(
        [0, 0, thickness - headHeight + 0.0001],
        cylinder({ d1: d, d2: dk + dkMargin, h: headHeight })
      )
    )
  })()
  const attachHole = translate(
    [0, 0, -0.001],
    cylinder({ d: 5.3, h: thickness + 0.001 })
  )

  // const doOneU = true
  const doOneU = params.doOneU

  const attachHoleYOffset = 26
  const attachHoleXOffset = doOneU ? 7 : 0 // offset the holes for oneu

  const height = doOneU ? oneU : threeUHeight
  const yBetweenHoles =
    (doOneU ? oneUOuterHeight : panelOuterHeight) - offsetToMountHoleCenterY * 2

  const attachHoles = union(
    translate(
      [width / 2 + attachHoleXOffset, attachHoleYOffset, 0],
      attachHole
    ),
    translate(
      [width / 2 - attachHoleXOffset, height - attachHoleYOffset, 0],
      attachHole
    )
  )

  return difference(
    cube({ size: [30, height, thickness], center: false }),
    translate(
      [13.8, (height - yBetweenHoles) / 2, 0],
      railsDrillHole,
      translate([0, yBetweenHoles, 0], railsDrillHole)
    ),
    attachHoles
  )
}

module.exports = {
  main,
  getParameterDefinitions: () => [
    { name: "doOneU", type: "checkbox", checked: false, caption: "OneU?" },
    {
      name: "thickness",
      type: "number",
      initial: 3.0,
      step: 0.1,
      caption: "Thickness?",
    },
  ],
}
