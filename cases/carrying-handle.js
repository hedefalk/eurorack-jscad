const jscad = require("jscad-tree-experiment")
const { translate } = jscad.api.transformations
const { cube, roundedCube, cylinder } = jscad.api.primitives3d
const { difference, union } = jscad.api.booleanOps
const { linear_extrude } = jscad.api.extrusions
const csg = jscad.api.csg
const {
  threeUHeight,
  panelOuterHeight,
  offsetToMountHoleCenterY
} = require("../eurorack-lib/constants")

const thickness = 10

const bezierHandle = (() => {
  // const fingerXDiam = 27
  const fingerXDiam = 27
  const fingerYDiam = 6
  const fingerXBezierOffset = 8

  const palmYControl = 35
  const palmY = 35
  totalWidth = fingerXDiam * 4
  const path = new csg.CSG.Path2D([[0, 0]])
    .appendBezier(
      [
        [0, -palmYControl],
        [totalWidth / 2, -palmY],
        [totalWidth, -palmYControl],
        [totalWidth, 0]
      ],
      {
        resolution: 100
      }
    )
    .appendBezier(
      [
        [totalWidth, fingerYDiam],
        [totalWidth - fingerXDiam / 2, fingerYDiam],
        [totalWidth - fingerXDiam, fingerYDiam],
        [totalWidth - fingerXDiam + fingerXBezierOffset, 0],
        [totalWidth - fingerXDiam, 0]
      ],
      {
        resolution: 100
      }
    )
    .appendBezier(
      [
        [totalWidth - fingerXDiam - fingerXBezierOffset, 0],
        [totalWidth - fingerXDiam, fingerYDiam],
        [totalWidth - fingerXDiam * 1.5, fingerYDiam],
        [totalWidth - fingerXDiam * 2, fingerYDiam],
        [totalWidth - fingerXDiam * 2 + fingerXBezierOffset, 0],
        [totalWidth - fingerXDiam * 2, 0]
      ],
      {
        resolution: 100
      }
    )
    .appendBezier(
      [
        [totalWidth - fingerXDiam * 2 - fingerXBezierOffset, 0],
        [totalWidth - fingerXDiam * 2, fingerYDiam],
        [totalWidth - fingerXDiam * 2.5, fingerYDiam],
        [totalWidth - fingerXDiam * 3, fingerYDiam],
        [totalWidth - fingerXDiam * 3 + fingerXBezierOffset, 0],
        [totalWidth - fingerXDiam * 3, 0]
      ],
      {
        resolution: 100
      }
    )
    .appendBezier(
      [
        [totalWidth - fingerXDiam * 3 - fingerXBezierOffset, 0],
        [totalWidth - fingerXDiam * 3, fingerYDiam],
        [totalWidth - fingerXDiam * 3.5, fingerYDiam],
        [totalWidth - fingerXDiam * 4, fingerYDiam],
        [0, 0]
      ],
      {
        resolution: 100
      }
    )
    .close()
    .innerToCAG()
  return linear_extrude({ height: thickness + 0.001 }, path)
})()

const primitiveHandle = (() => {
  const fingerDistance = 22
  const fingerDiam = 27

  const finger = cylinder({ d: fingerDiam, h: thickness + 0.0001 })
  const fingers = [0, 1, 2, 3].map(i =>
    translate([i * fingerDistance, 0, 0], finger)
  )
  const height = 23
  const totalWidth = fingerDistance * 3 + fingerDiam

  return union(
    translate([fingerDiam / 2, 0, 0], fingers),
    translate(
      [0, 0, -0.01],
      cube({ size: [totalWidth, height / 2, thickness + 0.02] }),
      csg.CSG.roundedCube({
        corner1: [0, 0, 0],
        corner2: [totalWidth, height, thickness + 0.02],
        roundradius: [20, 10, 0],
        resolution: 40
      })
    )
  )
})()

function main() {
  // return handle

  return difference(
    cube({ size: [200, 100, thickness], center: false }),
    translate(
      // [60, 40, 0],
      [45, 55, 0],
      // primitiveHandle
      bezierHandle
    )
  )
}

// Carrying handle routing template
module.exports = { main, getParameterDefinitions: () => [] }
