const jscad = require("jscad-tree-experiment")
const { translate } = jscad.api.transformations
const { cube, cylinder } = jscad.api.primitives3d
const { difference } = jscad.api.booleanOps
const { oneU } = require("../eurorack-lib/constants")

const width = 30 // just because the others were. Should be 30.1 for git-tec https://synthracks.com/eurorack-rails
const yBetweenHoles = 33.65

function main() {
  const thickness = 4.5
  const railsDrillHole = translate(
    [0, 0, -thickness + 0.001],
    cylinder({ d1: 0, d2: 10.5, h: 6 })
  )

  const attachHoleYOffset = 20
  const attachHoleXOffset = 7

  const attachHole = translate([0, 0, -0.001], cylinder({ d: 5, h: 10 }))
  return difference(
    cube({ size: [30, oneU, thickness], center: false }),
    translate(
      [width / 2, (oneU - yBetweenHoles) / 2, 0],
      railsDrillHole,
      translate([0, yBetweenHoles, 0], railsDrillHole)
    ),
    translate(
      [width / 2 + attachHoleXOffset, attachHoleYOffset, 0],
      attachHole
    ),
    translate(
      [width / 2 - attachHoleXOffset, oneU - attachHoleYOffset, 0],
      attachHole
    )
  )
}

module.exports = { main, getParameterDefinitions: () => [] }
