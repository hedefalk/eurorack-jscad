const jscad = require('jscad-tree-experiment')
const { translate, rotate } = jscad.api.transformations
const { cube, cylinder } = jscad.api.primitives3d
const { square, circle } = jscad.api.primitives2d
const { difference, union } = jscad.api.booleanOps
const { color } = jscad.api.color
const csg = jscad.api.csg
const { hp, oneU, threeUHeight } = require('../eurorack-lib/constants')
const { linear_extrude } = jscad.api.extrusions
// const OpenJsCad = jscad.api.OpenJsCad
// const openJscadAPI = require('@jscad/csg/api')
// const polygon = openJscadAPI.primitives2d.polygon

const caseDepth = 600
const caseHeight = 555

// First, do these two measure on the board, then draw a round line between the endpoints
const topDepth = 170 // depth at the top of rack
const frontHeight = 100 // height at the front

const woodThickness = 18 // https://www.bauhaus.se/hyllplan-svart-ek-18x600x2500mm-b-c-limfog
const rackHP = 126
const rackMm = rackHP * hp
const rackMountsThickness = 3
const innerWidth = rackMm + 2 * rackMountsThickness
const a = 0.0001 // offset to inhibit merging of objects

// actual pieces
const bottom = color('orange', cube({ size: [innerWidth, caseDepth - woodThickness, woodThickness], center: false }))
const back = color('lightgrey', cube({ size: [innerWidth, caseHeight - 2 * woodThickness - a, woodThickness], center: false }))
const top = color('lightblue', cube({ size: [innerWidth, topDepth, woodThickness], center: false }))
const front = color('lightgreen', cube({ size: [innerWidth, frontHeight, woodThickness], center: false }))

const side3d = (() => {
  const side2d = new csg.CSG.Path2D([[0, 0], [0, frontHeight]])
    .appendBezier([[caseDepth - topDepth, frontHeight], [caseDepth - topDepth, caseHeight]], { resolution: 100 })
    .appendPoint([caseDepth, caseHeight])
    .appendPoint([caseDepth, 0])
    .close()
    .innerToCAG()
  return linear_extrude({ height: woodThickness }, side2d)
})()

const moduleDepth = 50
const rail3 = cube({ size: [rackMm, threeUHeight, moduleDepth], center: false })
const rail1 = cube({ size: [rackMm, oneU, moduleDepth], center: false })

const angle = 90 / 5 // constant angles between the rows
const marginBetweenRow = 1

const rails = union(
  rail3,
  translate(
    [0, threeUHeight + marginBetweenRow, 0],
    rotate(
      [-angle, 0, 0],
      rail3,
      translate(
        [0, threeUHeight + marginBetweenRow, 0],
        rotate(
          [-angle, 0, 0],
          rail1,
          translate(
            [0, oneU + marginBetweenRow, 0],
            rotate(
              [-angle, 0, 0],
              rail3,
              translate(
                [0, threeUHeight + marginBetweenRow, 0],
                rotate([-angle, 0, 0], rail3, translate([0, threeUHeight + marginBetweenRow, 0], rotate([-angle, 0, 0], rail3)))
              )
            )
          )
        )
      )
    )
  )
)

const theCase = union(
  translate([0, -a, woodThickness], rotate([90, 0, 0], bottom)),
  translate([a, 0, caseDepth - woodThickness], back),
  translate([0, caseHeight - woodThickness + a, caseDepth - topDepth], rotate([90, 0, 0], top)),
  translate([0, -woodThickness, 0], front),
  translate([-a, -woodThickness, 0], rotate([0, -90, 0], side3d)),
  translate([innerWidth + woodThickness + a, -woodThickness, 0], rotate([0, -90, 0], side3d)),
  color([0, 0, 0, 0.7], translate([0, 80, woodThickness + marginBetweenRow], rotate([90, 0, 0], rails)))
)

const frontAndTop = union(top, translate([0, 180, 0], front))

const panelLayout = union(
  translate([0, 600, 30], rotate([0, 0, -90], color('grey', cube({ size: [600, 2000, 18], center: false })))),
  // bottom,
  translate([400, 200, 0], frontAndTop),
  // translate([910, 0, 0], rotate([0, 0, 90], side3d, translate([0, 1150, 0], rotate([180, 0, 0], side3d))), translate([30, 0, 0], back))
  translate([880, 0, 0], rotate([0, 0, 0], side3d)),
  translate([600, 0, 0], rotate([0, 180, 0], side3d))

  // , translate([0, 1150, 0], rotate([180, 0, 0], side3d))), translate([30, 0, 0], back))
)

function main() {
  return rotate([90, 0, 0], theCase, translate([900, 0, 0], panelLayout))
}

module.exports = { main, getParameterDefinitions: () => [] }
