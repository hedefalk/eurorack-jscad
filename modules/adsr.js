const { hp, eurorackPanel, PJ301, AlphaPot } = require('../eurorack-lib/eurorack-lib')

const jscad = require('jscad-tree-experiment')
const { difference, union } = jscad.api.booleanOps
const { translate, rotate, mirror } = jscad.api.transformations
const { cube } = jscad.api.primitives3d

const panelHp = 4
const width = hp * panelHp

const middle = width / 2

const jacks = [[0, 50.18564], [0, 137.78564]]

const pots = [[0, 62.2198], [0, 82.0198], [0, 101.8198], [0, 121.6198]]

function holes() {
  return translate(
    [0.3, 115, 0],
    mirror(
      [0, 1, 0],
      pots.map(pot => translate(pot, AlphaPot())),
      jacks.map(jack => translate(jack, rotate([0, 0, 180], PJ301())))
    )
  )
}

function reinforcements() {
  const reinforcement_width = 1
  const reinforcement_height = 7

  return translate(
    [middle - reinforcement_width / 2, 10, 0],
    cube({ size: [reinforcement_width, 108, reinforcement_height], center: false }),
    translate([-6, 0, 0], cube({ size: [reinforcement_width, 68, reinforcement_height], center: false })),
    translate([6, 0, 0], cube({ size: [reinforcement_width, 68, reinforcement_height], center: false })),
    translate([19.5, 0, 0], cube({ size: [reinforcement_width, 108, reinforcement_height], center: false })),
    translate([-19.5, 0, 0], cube({ size: [reinforcement_width, 108, reinforcement_height], center: false }))
  )
}

function main() {
  return difference(
    union(
      eurorackPanel(panelHp, { mountHoles: 2 })
      // reinforcements()
    ),
    translate([10.5, 85, 0], rotate([0, 0, 180], holes()))
  )
}

module.exports = { main, getParameterDefinitions: () => [] }
