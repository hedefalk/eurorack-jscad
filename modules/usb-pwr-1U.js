const { oneUEurorackPanel, usbTypeA, sideSupport } = require('../eurorack-lib')
const jscad = require('jscad-tree-experiment')
const { difference, union } = jscad.api.booleanOps
const { translate, rotate } = jscad.api.transformations

const cutouts = [translate([10, 20, 0], rotate([0, 0, 90], usbTypeA()))]

const solids = [oneUEurorackPanel(4, 2), sideSupport()]

function main() {
  if (false) { // eslint-disable-line
    return cutouts
  } else {
    return difference(union(solids), cutouts)
  }
}

module.exports = { main, getParameterDefinitions: () => [] }
