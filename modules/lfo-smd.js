const { hp, eurorackPanel, ToggleSwitchDPDT, ToggleSwitchSPDT, PJ301, RV09Pot, ThreeMmLed } = require('../eurorack-lib')
const jscad = require('jscad-tree-experiment')
const { difference, union } = jscad.api.booleanOps
const { translate, rotate } = jscad.api.transformations
const { cube } = jscad.api.primitives3d

const panelHp = 4

const width = hp * panelHp
const middle = width / 2

const jacks = [[0.3428, 103.251], [0.3428, 127.856], [0.3428, 140.9845], [10.4393, 140.9845]]

const rangeSwitch = [5.3603, 52.578]
const syncPolaritySwitch = [10.4393, 103.251]

const leds = [[10.4393, 127.856]]

const pots = [[5.37, 70.574]]

function holes() {
  return translate(
    [4.7, -30, 0],
    pots.map(pot => translate(pot, RV09Pot())),
    jacks.map(jack => translate(jack, rotate([0, 0, 180], PJ301({ extraCutouts: true, extraCutoutDepth: 1.5 })))),
    leds.map(led => translate(led, ThreeMmLed())),
    [translate(rangeSwitch, ToggleSwitchDPDT({ extraCutout: true }))],
    [translate(syncPolaritySwitch, ToggleSwitchSPDT())]
  )
}

function reinforcements() {
  const reinforcement_width = 1
  const reinforcement_height = 7

  //middle
  return translate(
    [middle - reinforcement_width / 2, 10, 0],
    translate([0, 40, 0], cube({ size: [reinforcement_width, 68, reinforcement_height], center: false })),
    translate([-9, 0, 0], cube({ size: [reinforcement_width, 50, reinforcement_height], center: false })),
    translate([9, 0, 0], cube({ size: [reinforcement_width, 50, reinforcement_height], center: false }))
  )
}

function main() {
  return difference(union(eurorackPanel(4, { mountHoles: 2 }), reinforcements()), holes())
}

module.exports = { main, getParameterDefinitions: () => [] }
