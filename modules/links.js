const { eurorackPanel, PJ301, ThreeMmLed } = require('../eurorack-lib')
const jscad = require('jscad-tree-experiment')
const { difference } = jscad.api.booleanOps
const { translate, rotate } = jscad.api.transformations

const left = 105
const right = 114.8

const jacks_180 = [
  [left, 58.78675],
  [right, 58.78675],
  [left, 96.77675],
  [right, 96.77675],
  [left, 132.5],
  [right, 132.5],
]

const jacks = [[left, 70.2], [right, 70.2], [left, 108.2], [right, 108.2], [left, 143.9], [right, 144]]

const leds = [[110.2111, 50.7], [110, 88.7], [110.1, 124.3]]

const cutouts = translate(
  [-100, -33, 0],
  jacks.map(jack => translate(jack, PJ301())),
  jacks_180.map(jack => translate(jack, rotate([0, 0, 180], PJ301()))),
  leds.map(led => translate(led, ThreeMmLed()))
)

function main() {
  return difference(eurorackPanel(4, 2), cutouts)
}

module.exports = { main, getParameterDefinitions: () => [] }
