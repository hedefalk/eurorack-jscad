const { eurorackPanel, PJ301 } = require('../eurorack-lib')
const jscad = require('jscad-tree-experiment')
const { difference } = jscad.api.booleanOps
const { translate, rotate } = jscad.api.transformations

const jacks = [
  [0, 56.133],
  [0, 68.624357],
  [0, 81.115714],
  [0, 93.607071],

  [0, 106.098428],
  [0, 118.589785],
  [0, 131.081142],
  [0, 143.572499],
]

const cutouts = translate([4.85, -36, 0], jacks.map(jack => translate(jack, rotate([0, 0, 0], PJ301()))))

function main() {
  if (false) {
    return cutouts
  } else {
    return difference(eurorackPanel(2, { mountHoles: 2 }), cutouts)
  }
}

module.exports = { main, getParameterDefinitions: () => [] }
