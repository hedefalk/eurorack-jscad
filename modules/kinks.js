const { eurorackPanel, PJ301, ThreeMmLed } = require('../eurorack-lib')
const jscad = require('jscad-tree-experiment')
const { translate, rotate } = jscad.api.transformations

const jacks = [
  [104.6486, 70.48675],
  [115.1736, 70.48675],

  [104.6486, 106.37675],
  [115.1736, 106.37675],

  [104.6486, 143.76675],
  [115.1736, 143.76675],
]

const jacks_180 = [
  [104.6486, 59.08675],
  [115.1736, 59.08675],

  [104.6486, 95],
  [115.1736, 95],

  [104.6486, 132.3],
  [115.1736, 132.3],
]

const leds = [[104.6486, 50.9], [104.6486, 86.9], [104.6486, 124.1]]

const cutouts = translate(
  [-100, -38, 0],
  jacks.map(jack => translate(jack, PJ301())),
  jacks_180.map(jack => translate(jack, rotate([0, 0, 180], PJ301()))),
  leds.map(led => translate(led, ThreeMmLed()))
)

function main() {
  return eurorackPanel(4, 2, cutouts)
}

module.exports = { main, getParameterDefinitions: () => [] }
