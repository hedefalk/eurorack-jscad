const { widthOfHp, eurorackPanel, PJ301, AlphaPot, ThreeMmLed } = require('../eurorack-lib/eurorack-lib')({
  panelThickness: 2.3,
})

const jscad = require('jscad-tree-experiment')
const { difference, union } = jscad.api.booleanOps
const { translate, rotate, mirror } = jscad.api.transformations
const { cube } = jscad.api.primitives3d
const { color } = jscad.api.color

const panelHp = 10
const width = widthOfHp(panelHp)

const middle = width / 2

const pcbTop = 51
const pcbLeft = 100.3
const pcbRight = 149.7
const pcbBottom = 160
const pcbMiddle = (pcbLeft + pcbRight) / 2

const coordToPcb = x => translate([middle - pcbMiddle, -41, 0], x)

const pcb = () =>
  color('grey', translate([pcbLeft, pcbTop, 4], cube({ size: [pcbRight - pcbLeft, pcbBottom - pcbTop, 1.6] })))

const [pots, trimmerHole] = (() => {
  const col1 = 113
  const col2 = 137
  const row1 = 57.557
  const row2 = 85.307
  const row3 = 112.582

  return [
    [[col1, row1], [col1, row2], [col1, row3], [col2, row1], [col2, row2], [col2, row3]],
    [(col1 + col2) / 2, row1],
  ]
})()

const [jacks, jacks_180] = (() => {
  const left = 104.9
  const right = 145.075
  const row1 = 139.757
  const row2 = 153

  return [
    [
      [left, row1],
      [left + ((right - left) * 1) / 4, row1],
      [left + ((right - left) * 2) / 4, row1],
      [left + ((right - left) * 3) / 4, row1],
      [right, row1],
    ],
    [[left, row2], [left + ((right - left) * 1) / 3, row2], [left + ((right - left) * 2) / 3, row2], [right, row2]],
  ]
})()

function holes() {
  return union(
    pots.map(pot => translate(pot, AlphaPot())),
    jacks.map(jack => translate(jack, PJ301({ extraCutouts: false }))),
    jacks_180.map(jack => translate(jack, rotate([0, 0, 180], PJ301({ extraCutouts: false })))),
    translate(trimmerHole, ThreeMmLed()) // Not led, but fuck it
  )
}

function main() {
  return difference(
    union(eurorackPanel(panelHp, { mountHoles: 4, slots: false }),
    //  coordToPcb(pcb())
    ),
    coordToPcb(holes())
  )
}

module.exports = { main, getParameterDefinitions: () => [] }
