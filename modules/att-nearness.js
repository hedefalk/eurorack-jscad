const { eurorackPanel, PJ301 } = require('../eurorack-lib/eurorack-lib')
const jscad = require('jscad-tree-experiment')
const { difference } = jscad.api.booleanOps
const { translate, rotate } = jscad.api.transformations

const jacks_180 = []

// const cutouts = translate([-100, -33, 0], jacks_180.map(jack => translate(jack, rotate([0, 0, 180], PJ301()))))

function main() {
  return difference(eurorackPanel(6, 2), [])
}

module.exports = { main, getParameterDefinitions: () => [] }
