const { hp, eurorackPanel, PJ301, AlphaPot } = require('../eurorack-lib/eurorack-lib')({ panelThickness: 2.3 })

const jscad = require('jscad-tree-experiment')
const { difference, union } = jscad.api.booleanOps
const { translate, rotate, mirror } = jscad.api.transformations
const { cube } = jscad.api.primitives3d
const { color } = jscad.api.color

const panelHp = 6
const width = hp * panelHp

const middle = width / 2

// const jacks = [[0, 50.18564], [0, 137.78564]]

const pcbMiddle = 149.825
const pcbTop = 37
const pcbLeft = 135.7
const pcbRight = 163.95
// const pcbBottom = 147

const pcb = () => color('grey', translate([pcbLeft, pcbTop, 4], cube({ size: [pcbRight - pcbLeft, 110, 1.6] })))

const pots = [[149.825, 42.925], [149.825, 65.1833], [149.825, 87.4417], [149.825, 109.7]]

const coordToPcb = x => translate([middle - pcbMiddle, -28, 0], x)

const jacks = [
  [140.325, 129.925],
  [149.812, 129.925],
  [159.3, 129.925],
  [140.325, 140.885],
  [149.812, 140.885],
  [159.3, 140.885],
]

function holes() {
  return union(
    pots.map(pot => translate(pot, AlphaPot())),
    jacks.map(jack => translate(jack, PJ301({ extraCutouts: false })))
  )
}

function main() {
  return difference(
    union(eurorackPanel(panelHp, { mountHoles: 4, slots: false }),
    //  coordToPcb(pcb())
    ),
    coordToPcb(holes())
  )
}

module.exports = { main, getParameterDefinitions: () => [] }
