const { eurorackPanel, PJ301, RV09Pot, TrimmerHole } = require('../eurorack-lib')
const jscad = require('jscad-tree-experiment')
const { difference } = jscad.api.booleanOps
const { translate, rotate } = jscad.api.transformations

const pots = [
  [140.021819, 60.456727],
  [140.021819, 80.456727],
  [140.021819, 100.456727],
  [140.021819, 120.456727],
  [140.021819, 140.456727],
]

const jacks_90 = [[156.521819, 60.456727], [156.521819, 80.456727], [156.521819, 100.456727], [156.521819, 120.456727]]
const jacks_180 = [[156.521819, 140.456727]]

const trimmer_holes = [[140.2, 72.7]]

const cutouts = translate(
  [-128, -38, 0],
  pots.map(jack => translate(jack, RV09Pot())),
  jacks_90.map(jack => translate(jack, rotate([0, 0, 90], PJ301()))),
  jacks_180.map(jack => translate(jack, rotate([0, 0, 180], PJ301()))),
  trimmer_holes.map(hole => translate(hole, TrimmerHole()))
)

function main() {
  return difference(eurorackPanel(8, 4), cutouts)
}

module.exports = { main, getParameterDefinitions: () => [] }
