const jscad = require('jscad-tree-experiment')
const { translate, rotate } = jscad.api.transformations
const { cube, cylinder } = jscad.api.primitives3d
const { square, circle } = jscad.api.primitives2d
const { difference, union } = jscad.api.booleanOps
const { color } = jscad.api.color
const csg = jscad.api.csg
// const { hp, oneU, threeUHeight } = require('../eurorack-lib/constants')
const { linear_extrude } = jscad.api.extrusions
// const OpenJsCad = jscad.api.OpenJsCad
// const openJscadAPI = require('@jscad/csg/api')
// const polygon = openJscadAPI.primitives2d.polygon

// led 3*5 och 4 för ringen
const photoCellWidth = 5.5
const photoCellHeight = 4.8
const photoCellThickness = 2.2

const ledDiam = 3.2
const ledLength = 5
const ledRingDiam = 4.0
const ledRingLength = 1.2

const ySize = 10
const xSize = 12
const zSize = 7


const photoCell = cube({size: [photoCellThickness, photoCellWidth, photoCellHeight], center: false})

const led = rotate([0, 90, 0], union(
  union(
    cylinder({d: ledDiam, h: ledLength, $fn: 40}), 
    translate([-photoCellHeight/2, -ledDiam/2, 0.0001], cube({size: [ledDiam/2, ledDiam+0.0001, ledLength], center: false}))
  ),
  translate([0, 0, ledLength], union(
    cylinder({d: ledRingDiam, h: ledRingLength, $fn: 40}),
    translate([ -photoCellHeight/2, -ledRingDiam/2, -0.0001], cube({size: [ledRingDiam/2, ledRingDiam+0.001, ledRingLength], center: false}))
    ))
  ))
const hollow = translate([1.8, 0, 0], union(
  translate([0, (ySize-photoCellWidth)/2 , (zSize-photoCellHeight)/2], photoCell),
  translate([photoCellThickness, (ySize/2), zSize/2], led)
))

const housing = cube({ size: [xSize, ySize, zSize], center: false, round: true})
function main() {
  const test = true
  if(!test) {
    return difference(housing, hollow)
  } else {
    return housing
    // return union(
    //   hollow,
    //   translate([0, 0, -7], housing)
    // )
  }
    

}

module.exports = { main, getParameterDefinitions: () => [] }
