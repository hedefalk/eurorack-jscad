// Specs from 
// http://www.doepfer.de/a100_man/a100m_e.htm
// https://intellijel.com/technical-specifications/

const hp = 5.08
const oneU = 44.45
const railHeight = 2.425

const threeUHeight = oneU * 3 //overall 3u height, 133.35
const marginForRails = railHeight * 2 // For both sides
const panelOuterHeight = threeUHeight - marginForRails // 128.5
const oneUOuterHeight = oneU - marginForRails

const railClearence = 9.25 // Fitting pcb and components within the rails
// const panelInnerHeight = panelOuterHeight - railClearence * 2 // 110 pcb size
const oneUPanelInnerHeight = oneUOuterHeight - railClearence * 2


// Should be 2mm, but 2.5 makes for lots of room for squish and sanding
// panelThickness = 2.5;
const panelThickness = 2.8
const panelThicknessAtRail = 2.2

// Trial and error
const m3 = 3.2

const mountHoleXMargin = 7.5 // http://www.doepfer.de/a100_man/a100m_e.htm

const offsetToMountHoleCenterY = 3


module.exports = {    
  hp, 
  oneU,
  m3, 
  mountHoleXMargin, 
  offsetToMountHoleCenterY,
  panelOuterHeight,
  oneUPanelInnerHeight,
  oneUOuterHeight,
  panelThickness,
  panelThicknessAtRail,
  railHeight,
  threeUHeight,
  railClearence,
}